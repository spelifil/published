import re
import queue 

# Node class 
class Hnode:
    def __init__(self, key, item):
        self.key = key
        self.item = item
        self.parent = None
        self.right = None
        self.left = None

    def getKey(self):
        return self.key

    def getItem(self):
        return self.item

    def getRightChild(self):
        return self.right

    def getLeftChild(self):
        return self.left

    def getParent(self):
        return self.parent

    def hasRightChild(self):
        return (self.right!=None)

    def hasLeftChild(self):
        return (self.left!=None)

    def isRoot(self):
        return (self.parent==None)

    def isLeaf(self):
        return ((self.right==None)and(self.left==None))

    def isRightChild(self):
        if self.isRoot() == True: return False
        return (self.parent.right==self)

    def isLeftChild(self):
        if self.isRoot() == True: return False
        return (self.parent.left==self)

    def setParent(self, p):
        self.parent=p

    def setKey(self, key):
        self.key = key

    def setItem(self, item):
        self.item = item
        
    def addRightChild(self, hnode):
        self.right=hnode
        hnode.setParent(self)

    def addLeftChild(self, hnode):
        self.left=hnode
        hnode.setParent(self)

    def getMinChild(self):
        if self.hasLeftChild() and self.hasRightChild():
            if self.left.key <= self.right.key:
                return self.left
            else:
                return self.right
        elif self.hasLeftChild():
            return self.left
        elif self.hasRightChild():
            return self.right
        return None
    
    def printInfo(self):
        print(f'|K: {self.key} I: {self.item}|')
        return

    def info(self):
        return f'|K: {self.key} I: {self.item}|'
    def removeChildren(self):
        if self.hasLeftChild():
            self.left.parent = None
            self.left = None
        if self.hasRightChild():
            self.right.parent = None
            self.right = None
    def getValues( self):
        return (self.key, self.item)

# Heap class 
class Heap:
    def __init__(self):
        self.root=None
        self.last=None
        self.size=0

    def isEmpty(self):
        return (self.size==0)

    def getSize(self):
        return self.size

    def getRoot(self):
        return self.root

    def getLast(self):
        q = queue.Queue()
        q.put( self.getRoot() )

        while( not q.empty() ):
            current = q.get()
            if current.hasLeftChild() and current.hasRightChild():
                q.put( current.left )
                q.put( current.right )
            else:
                return current
        return None

    def getHighestPriority(self):
        return self.root.getKey()

    def setRoot(self, hnode):
        self.root=hnode

    def setLast(self, hnode):
        self.last=hnode

#
# methods for heaps
#

    def swapNodes(self, a, b):
        tempKey = a.key
        tempItem = a.item

        a.key = b.key
        a.item = b.item

        b.key = tempKey
        b.item = tempItem


    #
    # downward adjustment from current node
    #
    def downwardHeapify(self,current):
        minChild = current.getMinChild()
        if minChild is not None and current.key > minChild.key:
            self.swapNodes(current, minChild)
            self.downwardHeapify(minChild)


    #
    # upward adjustment toward the root from the current node
    #
    def upwardHeapify(self, current : Hnode):
        if current.parent is not None and current.key < current.parent.key:
            self.swapNodes(current, current.parent)
            self.upwardHeapify(current.parent)


    #
    # The most important operation for a heap, remove_Min (extract_Min())
    #

    def getRightBottom(self):
        if self.getSize() == 0:
            return None

        q = queue.Queue()
        q.put(self.getRoot())
        last_node = None

        while not q.empty():
            current_node = q.get()
            last_node = current_node

            if current_node.hasLeftChild():
                q.put(current_node.getLeftChild())
            if current_node.hasRightChild():
                q.put(current_node.getRightChild())

        return last_node

    def removeMin(self):
        if self.getSize() == 0:
            return None
        minNode = self.getRoot()
        values = minNode.getValues()
        if self.getSize() == 1:
            self.setRoot(None)
            self.size -= 1
            return values
        
        lastNode = self.getRightBottom()
        # print(f'LN {lastNode.info()}')
        self.swapNodes(minNode, lastNode)
        # print(f'LN {lastNode.info()}')

        if lastNode.hasLeftChild():
            # print("C1")
            self.swapNodes(lastNode, lastNode.left)
            lastNode.removeChildren()
        elif lastNode.hasRightChild():
            # print("C2")
            self.swapNodes(lastNode, lastNode.right)
            lastNode.removeChildren()
        elif lastNode.isLeaf():
            if lastNode.isLeftChild():
                lastNode.parent.left = None
            elif lastNode.isRightChild():
                lastNode.parent.right = None
            else:
                raise ("badbadbad")
            lastNode.parent = None

        self.downwardHeapify(minNode)

        self.size -= 1
        return values



    #
    # Another important operation for a heap, insertion() (add())
    #
    def insert(self, hnode):
        if self.isEmpty():
            self.setRoot(hnode)
            self.size += 1
            return
        parent = self.getLast()
        
        if parent.hasLeftChild():
            parent.addRightChild(hnode)
        else:
            parent.addLeftChild(hnode)
        self.upwardHeapify(hnode)

        self.size += 1
        return

    #
    # For management, print the heap in pre-order
    #
    def printHeapPreOrder(self, nodeStart):
        nodeStart.printInfo()
        if nodeStart.hasLeftChild():
            self.printHeapPreOrder(nodeStart.getLeftChild())
        if nodeStart.hasRightChild():
            self.printHeapPreOrder(nodeStart.getRightChild())



# decimal to binary using yield
def d2by(x):
    while x>0 :
        x,r=divmod(x,2)
        yield r 

# function for starting the task
def HeapwithEntriesInserted():
    #
    # read the input information from the default input text file into an
    # entry list, entry_list
    #
    
    filename = 'inFile.txt'

    with open(filename) as file:
        entriesRaw = file.readlines()

    #
    # initiating a heap object h
    #
    h=Heap()

    #
    # Do something here to build the heap
    #
    for entryRaw in entriesRaw:
        if entryRaw[-1] == '\n':
            entryRaw = entryRaw[:-1]
        priority, item = entryRaw.split(' ', 1)
        h.insert( Hnode(int(priority), item) )


    #---------------------Print as the example on the homework sheet --------
    #                       will be adapted with the input
    #
    print("Heap size=", h.getSize(), "The highest priority is ", h.getHighestPriority())
    print("pre-order traversal:")
    h.printHeapPreOrder(h.getRoot())

    print("deleteMin")
    h.removeMin()

    print("deleteMin")
    h.removeMin()

    print("deleteMin")
    h.removeMin()

    print("deleteMin")
    h.removeMin()

    print("deleteMin")
    h.removeMin()

    print("insert 35, resume")
    h.insert(Hnode(35, "resume"))
    
    print("insert 15, second")
    h.insert(Hnode(15, "second"))

    print("insert 20, fourth")
    h.insert(Hnode(20, "fourth"))

    print("Heap size=", h.getSize(), "The highest priority is ", h.getHighestPriority())
    print("pre-order traversal:")
    h.printHeapPreOrder(h.getRoot())
    
    print("deleteMin")
    h.removeMin()

    print("insert 40, nineth")
    h.insert(Hnode(40, "nineth"))


h = Heap()

h.insert(Hnode(1, 1203))

assert( h.getHighestPriority() == 1 )

h.insert(Hnode(2, 1203))
h.insert(Hnode(3, 1203))
h.insert(Hnode(4, 1203))
assert( h.getHighestPriority() == 1 )
h.insert(Hnode(5, 1203))
h.insert(Hnode(6, 1203))
h.insert(Hnode(7, 1203))

print("order")
h.printHeapPreOrder(h.getRoot())
assert( h.removeMin() == (1, 1203))
assert( h.getHighestPriority() == 2 )
print("order")
h.printHeapPreOrder(h.getRoot())

assert( h.removeMin() == (2, 1203))
assert( h.getHighestPriority() == 3 )

assert( h.removeMin() == (3, 1203))
assert( h.getHighestPriority() == 4 )

h.insert(Hnode(1, 1203))

assert( h.getHighestPriority() == 1 )
assert( h.removeMin() == (1, 1203))
assert( h.getHighestPriority() == 4 )

print("order")
h.printHeapPreOrder(h.getRoot())
assert( h.removeMin() == (4, 1203))
assert( h.getHighestPriority() == 5 )

print("order")
h.printHeapPreOrder(h.getRoot())

assert( h.removeMin() == (5, 1203))
assert( h.getHighestPriority() == 6 )

assert( h.removeMin() == (6, 1203))
assert( h.getHighestPriority() == 7 )

assert( h.getSize() == 1)

assert( h.removeMin() == (7, 1203))
assert( h.isEmpty() )

# original test
HeapwithEntriesInserted()