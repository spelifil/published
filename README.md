# Filip Špelina, published works with source code

## What is this?

In this repository I publish some of my 'papers' and asignments. Mainly so I could point to a reachable source code of such work when required.

## License

Every work in this repository uses CC BY 4.0. As long as you credit me, you are free to do as you like. Credit is required as this may include my school work and for that I need to be known as the original author.

Published Work © 2023 by Filip Spelina is licensed under CC BY 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

