% Filip Spelina, 2023
% I used \OpTeX macro package for \TeX. A \LaTeX alternative I prefer. Included in tex-live.
% To compile, run: `optex spelifil_hw1/tex`
% https://petr.olsak.net/optex/

\useoptex
\load[pseudocode]

\fontfam[lm]
\typosize[11/14]
\parindent=1em
\hyperlinks \Blue \Green

\def\npage{\vfill\break\relax}

\picdir={img/}
\picwidth=\hsize

% ---------------

\tit Computer Algorithms HW1
\centerline{Filip Spelina}
\centerline{112012064}




\sec Square root

In order to calculate $\lfloor \sqrt{n} \rfloor$ we could use Newton's method which approximates the right output rather precisely.\cite[mitnewton]

\secc English description

When we get an positive integer $n$ we would estimate an initial guess to be ${n \over 2}$. 

Afterwards, we need to improve this guess. We will use a formula from the Newton's method. Let's say $x_n$ is our current guess and $x_{n+1}$ is the next guess. Using formula
$$
x_{n+1} = {1 \over 2} \times \left( x_n + {n \over x_n } \right)
$$
repeatably would give us better and better approximation of $\sqrt{n}$. The convergence to the proper result is {\bf quadratic}\cite[mitnewton]. Thanks to that we do not need to do this many times. Just seven iterations produces 60 accurate digits for $\sqrt{2}$. To be safe we could do the calculation 20 times.

Our algorithm for computing $\lfloor \sqrt{n} \rfloor$ would look like this then:

\begitems \style n
* Make an initial guess ${n \over 2}$
* Input the guess into the formula.
* Save the new guess as current one. 
* Repeat step {\bf 1} and {\bf 2} twenty times.
* Floor the current guess.
\enditems

\noindent
And now we have $\lfloor \sqrt{n} \rfloor$ for any positive $n$. 

\secc Pseudocode
\vbox{
\begtt \algol
Algorithm squareRoot$(n)$
    Input: positive integer $n$
    Output: $\lfloor \sqrt{n} \rfloor$
    $x \leftarrow {n \over 2}$
    for $i \leftarrow 0$ to $20$ do
        $x \leftarrow {1 \over 2} \times \left( x + {n \over x } \right)$
    end for
    $x \leftarrow \lfloor x \rfloor $
    return $x$
\endtt
}



\npage
\sec Euclid's algorithm \cite[stanfordeuclid]

We could use recursion as the algorithm is based on repeating the computations with smaller and smaller numbers until a simple case is found.

\secc Pseudocode

\begtt \algol
Algorithm gcd$(a, b)$
    Input: $a, b \in {\bbchar N}$
    Output: greatest common divider of $a$ and $b$
    while $a - b \ge 0$ do
        $a \leftarrow a - b$
    end while
    if $a = 0$ then  
        return $b$
    end if
    return gcd$(b, a)$
\endtt

\secc Euclid's game

From the very start, when the original pair of numbers is chosen, we can calculate the set of numbers created will all possible subtractions. The created numbers are all multiples of the greatest common divider of original numbers up to the largest number. 
$$10, 4 \rightarrow \left\{10, 8, 6, 4, 2 \right\} \leftarrow \gcd = 2$$
$$44, 11 \rightarrow \left\{44, 33, 22, 11 \right\} \leftarrow \gcd = 11$$
We can calculate the number of possibilities by dividing the larger number with the {\bf gcd}.

Two numbers of this set are already specified in the beginning. Because of that we need to subtract 2 from the size of the number set to get the number of possible moves. If the number of possible moves is {\bf odd} then you should play first. In case it's {\bf even} than play second.

In pseudocode this would look like this somehow.

\begtt \algol
Algorithm chooseStrategy$(a, b)$
    Input: two unequal natural numbers, $a > b$
    Output: whether you should play first or second
    numbersCount $\leftarrow {a \over \gcd(a, b)}$
    moves $\leftarrow {\rm numbersCount}- 2$
    if moves is odd then
        return PLAY FIRST 
    else moves is even then
        return PLAY SECOND
    end if 
\endtt

Actually, since subtracting 2 does not change the possible oddity, we can see the strategy from the numbersCount already.




\npage
\sec Sequence proof

\def\sequenceFile{3_sequenceProof_56_crop.pdf}

As far as I know there are different definitions for a decreasing sequence. I will use the following one that is known to me from my previous courses.

\medskip
{\bf Definition:} We say that a sequence $(a_n)$ is {\bf decreasing}, iff $a_n \geq a_{n+1}$ for every $n \in {\bbchar N}$
\medskip

First I managed to reduce the the problem to a suggested smaller one. After that I used mathematical induction
to prove $\left(1+{1 \over n}\right)^n \leq n$ for every $n \geq 3$.

\picw 0.9\hsize
\picparams={page1}
\centerline{\inspic{\sequenceFile}}

\bigskip

\picparams={page2}
\centerline{\inspic{\sequenceFile}}




\npage
\sec Induction and fibonacci sequence

\def\fibonacciFile{4_fibonacciProof_456_crop.pdf}

First of all I needed to somehow intuitively confirm that every integer can really be described as the sum of not consecutive fibonacci numbers.

\picw=\hsize
\picparams={page2}\inspic{\fibonacciFile}

My thinking was that if I can construct an algorithm that would accept an integer and could output it's decomposition to
nonconsecutive fibonacci numbers for every integer, then the possibility would be proven.

Using recursion in the algorithm seems useful thanks to the similarities to mathematical induction I would need to do. Here I assume we are able to
find the largest fibonacci number lesser or equal to $n$ with findLargestFib() function.

\begtt \algol
Algorithm fibonacciDecomposition$(n, A)$
    Input: natural number $n$ and empty list $A$
    Output: list $A$ contains the decomposition of $n$
    $x \leftarrow findLargestFib(n)$
    $A.append(n)$
    if $n == x$ then
        return $A$
    else
        return fibonacciDecomposition$(n-x), A$
    end if
\endtt

Here are some more of mine thoughts on the method.

\picparams={page3}\inspic{\fibonacciFile}

Mathematical induction can be used to proof the correctness of an algorithm.\cite[ottawaInduction] In the following mathematical induction I use similar methods
from the algorithm to try my best to show that with decompositions of lesser numbers I can always make one for greater number.

\picparams={page1}\inspic{\fibonacciFile}

\npage
\nonum\sec Sources

\usebib/c (iso690) ComAlg_hw1

\nonum\secc Source code

This is written with \OpTeX/, a \TeX/ macro package. 
The source code of this pdf can be found at \url{gitlab.fit.cvut.cz/spelifil/published}. My personal gitlab repository supplied by CTU in Prague, it is connected to my name.

\bye