import numpy as np 

def gcdEuclid(a : int, b : int):
    while (a - b >= 0):
        a = a - b
    if (a == 0):
        return b
    return gcdEuclid( b, a )


np.random.seed(0)
testdata = np.random.randint(1, 10000, size=(1000, 2))

gcmNumpy = np.gcd.reduce(testdata, axis=1)

# print(testdata[314])

pairnum = 0
for pair in testdata:
    # print(pairnum)
    euclid = gcdEuclid(pair[0], pair[1])
    if (euclid == gcmNumpy[pairnum]):
        pairnum += 1
        continue
    pairnum += 1
    print("ERROR")
    print(pair)

print( gcdEuclid( 546, 1785 ))

print("end")

# ----------------------
print("fibtest")

# We are creating an array contains n = 10 elements 
# for getting first 10 Fibonacci numbers 
a = np.arange(1, 90) 
  
# splitting of terms for easiness 
sqrtFive = np.sqrt(5) 
alpha = (1 + sqrtFive) / 2
beta = (1 - sqrtFive) / 2
  
# Implementation of formula 
# np.rint is used for rounding off to integer 
Fn = np.rint(((alpha ** a) - (beta ** a)) / (sqrtFive))
fibInt = Fn.astype(int)
print(fibInt)
def findLargestFib(n :int) -> int:
    return np.max(fibInt[fibInt <= n])
    
print(findLargestFib(7))
print(findLargestFib(8))
print(findLargestFib(340))
print(findLargestFib(5000))


def fibonacciDecomposition (n : int, A : list) -> list :
    x = findLargestFib(n)
    A.append(x)
    if n == x:
        return A
    return fibonacciDecomposition(n - x, A)
print( sum(fibonacciDecomposition(7, [])), fibonacciDecomposition(7, [])    )
print( sum(fibonacciDecomposition(8, [])), fibonacciDecomposition(8, [])    )
print( sum(fibonacciDecomposition(340, [])), fibonacciDecomposition(340, [])  )
print( sum(fibonacciDecomposition(5000, [])), fibonacciDecomposition(5000, []) )
print( sum(fibonacciDecomposition(54, [])), fibonacciDecomposition(54, []) )


# % $$
# % {\bf Definition: } {\rm We say that a sequence} (a_n) {\rm is} {\bf decreasing}, {\rm iff} a_n ≥ a_{n+1} {\rm for every} n ∈ {\bbchar N}.
# % $$