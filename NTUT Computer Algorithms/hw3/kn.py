def findLowestWeight(stuff, type):
    min = 999999999999
    for thing in stuff:
        if thing[2] == type and thing[0] < min:
            min = thing[0]
    return min

def recKnap(stuff :list , value, weight, maxWeight, blue, red, loot :list) -> (int, list):
    bestValue = value
    bestLoot = loot.copy()
    for thing in stuff:
        # print(thing)
        if blue != 0 and red != 0:
            if thing[2] == 'b' and red == 0:
                continue
            if thing[2] == 'r' and blue == 0:
                continue
        if blue == 0 and red == 0:
            if thing[2] == 'b' and thing[0] + findLowestWeight(stuff, 'r') > maxWeight:
                continue
            if thing[2] == 'r' and thing[0] + findLowestWeight(stuff, 'b') > maxWeight:
                continue

        if (thing[0] + weight ) > maxWeight:
            continue
        
        if thing[2] == 'b':
            blue += 1
        if thing[2] == 'r':
            red += 1

        newLoot = loot.copy()
        stuff.remove(thing)
        # print( "THING", thing, blue, red)
        # print( "STUFF", stuff)
        # print( "LOOT", loot)
        # print("\n")
        newLoot.append(thing)
        # print(thing, loot)
        bestValueUnder, newLoot = recKnap(stuff, thing[1] + value, thing[0] + weight, 
                                                        maxWeight, blue, red, newLoot)
        # print(thing, bestValueUnder, loot)
        if bestValueUnder > bestValue:
            bestValue = bestValueUnder
            bestLoot = newLoot

        if thing[2] == 'b':
            blue -= 1
        if thing[2] == 'r':
            red -= 1
        
        stuff.append(thing)

    return bestValue, bestLoot

def knapsackStart(stuff : list, maxWeight):
    print("START", maxWeight, stuff)
    bestVal, loot = recKnap(stuff, 0, 0, maxWeight, 0, 0,[])
    if len(loot) < 2:
        raise ValueError("At least two object should be present")
    return bestVal, loot

stuff = [[5, 10, 'b'], [6, 7, 'b'], [4, 8, 'r'], [1, 4, 'r'], [10, 100, 'b']]
W = 10

print("FINAL RESULT",knapsackStart(stuff, W))