import numpy as np
import pandas as pd
stuff = [[0,0, 'X'],[5, 10, 'b'], [6, 7, 'b'], [4, 8, 'r'], [1, 4, 'r'], [10, 100, 'b']]
W = 10

def dynKnap(stuff : list, maxWeight : int ) -> int:
    blue = np.zeros(shape=(len(stuff), maxWeight+1), dtype=bool)
    red = np.zeros(shape=(len(stuff), maxWeight+1), dtype=bool)
    dynTable = np.zeros(shape=(len(stuff), maxWeight+1))
    dynTable = pd.DataFrame(dynTable)
    for i in range ( len(stuff) ):
        for w in range( maxWeight+1 ):
            # base case -> zero items or zero weight requirment
            if i == 0 or w == 0:
                dynTable[i][w] = 0
            # when item fits into weight
            prevBest = dynTable.iloc[i-1, w]
            prevRed = red[i-1, w]
            prevBlue = blue[i-1, w]
            curBlue, curRed = False, False
            if stuff[i][2] == 'b': curBlue = True
            if stuff[i][2] == 'r': curRed = True
            if stuff[i][0] <= w:
                used = stuff[i][1] + dynTable.iloc[i-1, w - stuff[i][0]]
                # value is USED
                if used > prevBest:
                    dynTable.iloc[i, w] = used
                    red[i, w] = curRed | red[i-1,  w - stuff[i][0]]
                    blue[i, w] = curBlue | blue[i-1,  w - stuff[i][0]]
                else:
                    # value is worse than best before
                    dynTable.iloc[i, w] = prevBest
                    red[i, w] = prevRed
                    blue[i, w] = prevBlue
            else:
                # value does not fit
                dynTable.iloc[i, w] = prevBest
                red[i, w] = prevRed
                blue[i, w] = prevBlue
    print(dynTable)
    # find best -> red blue true and max
    best = 0
    for i in range ( len(stuff) ):
        for w in range( maxWeight+1 ):
            if dynTable.iloc[i, w] > best and red[i, w] == True and blue[i, w] == True:
                best = dynTable.iloc[i,w]
    print(best)


dynKnap(stuff, W)