import random
COUNT = 10
random_list = random.sample(range(1, COUNT+1), COUNT)
print(random_list)
permutation = random_list

# permutation = [2, 4, 1, 3, 5]
order = permutation.copy()
order.sort()

# creates a list of permutation pairs
def makeLinkPairs(permutation):
    links = []
    for number in order:
        links.append([number, permutation.index(number) + 1])
    return links

# evaluates to true when given pairs cross
def isCrossed(pair1, pair2):
    interval1 = sorted(pair1)
    interval2 = sorted(pair2)

    if interval1[1] > interval2[0] and interval2[1] > interval1[0]:
        return True  
    else:
        return False

# evaluates to true if any of given pairs cross
def isAnyCrossed(links):
    if(len(links)) <= 1:
        return False
    for linkA in links:
        for linkB in links:
            if linkA is not linkB and isCrossed(linkA, linkB):
                return True
    return False

# returns the maximum independent subset of permutation pairs
def maxIndependentSubset(links):
    if isAnyCrossed(links) is False:
        return links
    
    maxSize = 0
    maxSubset = []
    for link in links:
        links.remove(link)
        newMaxSubset = maxIndependentSubset(links)
        newMaxSize = len (newMaxSubset)
        if newMaxSize > maxSize:
            maxSize = newMaxSize
            maxSubset = newMaxSubset.copy()
        links.append(link)
    return maxSubset

assert( isCrossed([1,3], [2, 1]) ) 
assert( isCrossed([1,3], [4, 2]) ) 
assert( isCrossed([3,4], [4, 2]) ) 
assert( not isCrossed([5,5], [1, 3]) ) 
assert( not isCrossed([5,5], [4, 2]) )
assert( not isCrossed([5,5], [3, 4]) ) 
assert( not isCrossed([5,5], [2, 1]) ) 
assert( not isCrossed([4,2], [2, 1]) ) 
assert( not isCrossed([1,3], [3, 4]) ) 

assert( isAnyCrossed([ [1,3], [2,1], [4,2] ]))
assert( isAnyCrossed([ [1,3], [3,4], [5,5], [2,1] ]))
assert( isAnyCrossed( makeLinkPairs(permutation) ))
assert( not isAnyCrossed([ [1,3], [3,4], [5,5] ]))
assert( not isAnyCrossed([[1,3]]) ) 


links = makeLinkPairs(permutation)
print(maxIndependentSubset(links))